﻿ssi.d3.areaChart = (function () {
    var initModule, load, clear, showLegend,
        getColorDomain,
        config = {
            container: null,
            chart : null
        };
    var group1 = ['BURGLARY',
                      'ASSAULTS',
                      'WEAPONS CALLS',
                      'PROSTITUTION',
                      'FAILURE TO REGISTER (SEX OFFENDER)',
                      'ROBBERY',
                      'THREATS, HARASSMENT',
                      'ARREST',
                      'PERSON DOWN/INJURY',
                      'PERSONS - LOST, FOUND, MISSING'],
          group2 = ['AUTO THEFTS',
                        'ACCIDENT INVESTIGATION',
                        'CAR PROWL',
                        'SHOPLIFTING',
                        'PROPERTY DAMAGE',
                        'TRESPASS',
                        'PROPERTY - MISSING, FOUND',
                        'OTHER PROPERTY',
                        'BIKE',
                        'MOTOR VEHICLE COLLISION INVESTIGATION'],
          group3 = ['LIQUOR VIOLATIONS',
                        'MISCELLANEOUS MISDEMEANORS',
                        'NARCOTICS COMPLAINTS',
                        'HARBOR CALLS',
                        'MENTAL HEALTH',
                        'RECKLESS BURNING',
                        'SUSPICIOUS CIRCUMSTANCES',
                        'LEWD CONDUCT',
                        'HAZARDS',
                        'ANIMAL COMPLAINTS',
                        'PUBLIC GATHERINGS'],
          group4 = [];

    getColorDomain = function (d) {
        if (d && d.key) {
            if (group1.indexOf(d.key[0]) >= 0) {
                return 1;
            }
            else if (group2.indexOf(d.key[0]) >= 0) {
                return 2;
            }
            else if (group3.indexOf(d.key[0]) >= 0) {
                return 3;
            }
            else {
                return 4;
            }
        }

        return -1;
    }

    load = function () {
        clear();
        $('#legendBtn').on('click', showLegend);

        var colorsGen = d3.scale.ordinal().domain([1, 4])
        .range([
            '#a50f15', 
            '#cb181d', 
            '#fb6a4a', 
            '#f16913'
        ])
        //.range([
        //    '#fff5f0', '#fee0d2', '#fcbba1', '#EF343E', '#fc9272', '#fb6a4a', '#ef3b2c', '#cb181d', '#a50f15', '#67000d',
        //    '#f7fbff', '#deebf7', '#c6dbef', '#9ecae1', '#6baed6', '#4292c6', '#275776', '#2171b5', '#08519c', '#08306b',
        //    '#fff5eb', '#fee6ce', '#fdd0a2', '#fdae6b', '#fd8d3c', '#f16913', '#d94801', '#a63603', '#953002', '#7f2704',
        //    '#f2f0f7', '#dadaeb', '#bcbddc', '#9e9ac8', '#807dba', '#6a51a3', '#4a1486'
        //])
//      .range(["#fdae6b", "#fd8d3c", "#f16913", "#d94801", "#a63603", "#e77685",
//"#7ad751", "#d807fe", "#1eff06", "#d443e9", "#9758e7", "#dbb4fc",
//"#7fcdbb", "#41b6c4", "#1d91c0", "#225ea8", "#253494", "#081d58",
//"#fa9fb5", "#f768a1", "#dd3497", "#ae017e", "#7a0177", "#49006a",
//"#addd8e", "#78c679", "#41ab5d", "#238443", "#006837", "#004529",
//"#bdbdbd", "#969696", "#737373", "#525252", "#252525", "#000000",
//"#a8ddb5", "#7bccc4", "#4eb3d3", "#b0e3e8", "#0868ac", "#d0d1e6",
//"#a6bddb", "#74a9cf", "#3690c0", "#0570b0"]);

        
        nv.addGraph(function () {
            config.chart = nv.models.stackedAreaChart()
                .useInteractiveGuideline(true)
                .x(function (d) {
                    return parseInt(d.H)
                })
                .y(function (d) {
                    return d.x
                })
                .controlLabels({ stacked: "Stacked" })
                .color(function (d, i) {
                    var index = getColorDomain(d);
                    return colorsGen(index);
                })
                .interpolate('monotone')
                .duration(300);

            config.chart.xAxis
                .axisLabel('Hour of day (24 hour format)')
                .tickFormat(function (d) {
                    return d;
                });

            config.chart.yAxis
                .axisLabel('Crime count')
                .tickFormat(d3.format(',d'));

            config.chart.legend.vers('furious');
            var srcJSONPath = "static/data/processed.json";
            d3.json(srcJSONPath, function (error, json) {
                if (error) {
                    console.log('Error');
                    console.log(error);
                    return;
                }

                d3.select('#chart1')
                .datum(json)
                .transition().duration(1000)
                .call(config.chart)
                .each('start', function () {
                    setTimeout(function () {
                        d3.selectAll('#chart1 *').each(function () {
                            if (this.__transition__)
                                this.__transition__.duration = 1;
                        })
                    }, 0)
                });
            });


            nv.utils.windowResize(config.chart.update);
            return config.chart;
        });
    }

    clear = function () {
        $('#chart1').empty();
        $('#legendBtn').off('click', showLegend);
        config.chart = null;
    }

    showLegend = function (e) {
        var exp = config.chart.showLegend();
        config.chart.showLegend(!exp);
        config.chart.update();
    }

    initModule = function(container) {        
        config.container = container;
    }

    return{
        initModule : initModule,
        load: load,
        clear: clear

    }
})();