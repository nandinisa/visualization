ssi.d3.barChart = (function () {

    var initModule,
        config = {
            container: null,
            mode: null,
            data: null
        },
        plotBars, plotDetailedBar, clear, redraw, draw, type;

    var margin = { top: 20, right: 20, bottom: 30, left: 40 },
        width = 1500 - margin.left - margin.right,
        height = 1000 - margin.top - margin.bottom;

    var dateFormat = d3.time.format("%Y-%m-%d", 'PDT');

    var dateFilter = function (d) {
        var year = d['Event.Clearance.Date'].getFullYear();
        var month = d['Event.Clearance.Date'].getMonth();
        if (month < 10) {
            if (month == 0) {
                month = '00';
            }
            else {
                month = '0' + month;
            }
        }

        return (year + '-' + month);
    }

    var displayMonth = function (dMonthZeroBased) {
        var month = parseInt(dMonthZeroBased);
        var desc = ['Jan', 'Feb', 'March', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];        
        return desc[month];
    }

    var agg_year = function (leaves) {
        var total = leaves.length;
        return {
            'count': total
        };
    }

    var districtFilter = function (d) {
        if (d[config.mode] != null || d[config.mode].trim() != '') {
            return d[config.mode];
        }
        return 'null';
    }

    var dateBifurcation = function () {
        var ticks = [];
        for (var i = 2011; i < 2016; i++) {
            for (var j = 0; j <= 9; j++) {
                ticks.push(i + '-0' + j);
            }
            for (var j = 10; j < 12; j++) {
                ticks.push(i + '-' + j);
            }
        }

        return ticks;
    };

    plotBars = function (data) {
        config.data = data;

        if (config.mode == 'all') {
            var nested = d3.nest()
                            .key(dateFilter)
                            .sortKeys(d3.ascending)
                            .rollup(agg_year)
                            .entries(data);

            plotDetailedBar(nested, 2, true);
        }
        else {
            var firstNesting = d3.nest()
                               .key(districtFilter)
                               .sortKeys(d3.ascending)
                               .rollup(
                                    function (leaves) {
                                        return d3.nest()
                                            .key(dateFilter)
                                            .sortKeys(d3.ascending)
                                            .rollup(agg_year)
                                            .entries(leaves);
                                    }
                               )
                               .entries(data);
            var length = firstNesting.length;
            // each district
            firstNesting.map(function (d) {
                var topRow = d.key == 1;
                plotDetailedBar(d, length, topRow);
            });
        }
    }

    plotDetailedBar = function (d, length, topRow) {
        var parentDiv = d3.select(config.container)
                .append('div')
                .attr('id', 'barChart' + d.key)
                .attr('class', 'barContainer');

        // height of each district graph
        var barHeight = Math.floor(height / length);

        var svg = parentDiv.append("svg")
           .attr("width", width)
           .attr("height", barHeight)
         .append("g");

        var xScale = d3.scale.ordinal()
            .rangeRoundBands([0, width - 200], .05);

        var yScale = d3.scale.linear()
            .range([barHeight, 0]);

        var xAxis = d3.svg.axis()
            .scale(xScale)
            .orient("bottom")
            .tickFormat("");


        var yAxis = d3.svg.axis()
            .scale(yScale)
            .orient("left")
            .ticks(10);

        // get values for district key : ex: district 1
        var nested = d.values == null ? d : d.values;

        xScale.domain(dateBifurcation());

        yScale.domain([0, d3.max(nested, function (d) {
            return d.values['count'];
        })]);

        svg.append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + barHeight + ")")
          .call(xAxis);


        svg.append("g")
          .attr("class", "y axis")
          .call(yAxis)

        // Create groups for each series, rects for each segment 
        var groups = svg.selectAll("g.cost")
          .data(nested)
          .enter().append("g")
          .attr("class", "cost");

        // Prep the tooltip bits, initial display is hidden
        var tooltip = svg.append("g")
          .attr("class", "barTooltip")
          .style("display", "none");        

        tooltip.append("rect")
          .attr("width", 120)
          .attr("height", 70)
          .attr("fill", "white")
          .style("opacity", 0.8);


        
        tooltip.append("text")
          .attr("x", 15)
          .attr("dy", "1.2em")
          .attr("font-size", "12px")
          .attr("font-weight", "bold")
          .attr('font-color', 'black');

        // creating bars
        groups.selectAll("rect")
            .data(nested)
          .enter().append("rect")
            .attr("class", "crimeCount")
            .attr("x", function (p) {
                return xScale(p.key);
            })
            .attr("width", xScale.rangeBand())
            .attr("y", function (p) {
                return (yScale(p.values['count']));
            })
            .attr("height", function (p) {
                return barHeight - yScale(p.values['count']);
            })
            .on("dblclick", function (p) {
                clear(false);
                config.mode = 'district';
                plotBars(config.data)
            })
            .on("mouseover", function () {
                d3.select(this).style('cursor', 'pointer');
                tooltip.style("display", null);                
            })
            .on("mouseout", function () {
                tooltip.style("display", "none");
                d3.select(this)
                        .style("cursor", "hand");
            })
            .on("mousemove", function (p) {
                var xPosition = d3.mouse(this)[0] - 25;
                var yPosition = 0 + yScale(p.values['count']);
                var text = p.key.split('-');
                var displayText = [];
                displayText.push("Year : " + text[0]);
                displayText.push("Month : " + displayMonth(text[1]));
                displayText.push("Crime Count : " + p.values['count']);

                var text = tooltip.select('text'),
                yAttr = text.attr("y"),
                lineHeight = 0.2,
                dy = parseFloat(text.attr("dy"));

                tooltip.attr("transform", "translate(" + xPosition + "," + yPosition + ")");
                tooltip.selectAll('tspan').remove();
                //http://bl.ocks.org/mbostock/7555321
                $.each(displayText, function (i, t) {
                    //var tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
                    var tspan = text.append("tspan").attr("x", 0).attr("y", yAttr).attr("dy", ++i * lineHeight + dy + "em").text(t);
                });
            });

        // Create the linear line
        var smoothLine = d3.svg.line()
            .interpolate('basis')
            .x(function (p) {
                return xScale(p.key);
            })
            .y(function (p) {
                return yScale(p.values['count']);
            });
        
        // Draw smoothing line
        var lines = svg.append("g")
                .attr("class", "trends");

        lines
            .append("path")
            .attr("class", "smoothLine")
            .attr("d", smoothLine($(nested).map(function (i, d) {
                if (d.key.indexOf('2012') >= 0 || d.key.indexOf('2011') >= 0) {
                    return d;
                }
            })));

        lines
            .append("path")
            .attr("class", "smoothLine")
            .attr("d", smoothLine($(nested).map(function (i, d) {
                if (d.key.indexOf('2014') >= 0 || d.key.indexOf('2015') >= 0) {
                    return d;
                }
            })));

        //line bifurcation
        var year = 2011;
        for (var i = 0; i < 5 ; i++) {
            svg.append("line")
               .attr("x1", xScale((year + i) + '-11') + xScale.rangeBand())
               .attr("x2", xScale((year + i) + '-11') + xScale.rangeBand())
               .attr("y1", 0)
               .attr("y2", barHeight)
               .attr('class', 'bifurcation')

            svg.append("line")
               .attr("x1", xScale((year + i) + '-05') + xScale.rangeBand())
               .attr("x2", xScale((year + i) + '-05') + xScale.rangeBand())
               .attr("y1", 0)
               .attr("y2", barHeight)
               .style("stroke-dasharray", "3,10")
               .attr('class', 'bifurcation')

            if (topRow == true) {
                var yearLabel = svg.append("g")
                  .attr("transform", "translate(" + (xScale((year + i) + '-00') + 5) + "," + 10 + ")")
                  .attr("class", "yearLabel");

                yearLabel.append("text")
                    .attr("dy", ".35em")
                    .attr("font-size", "12px")
                    .attr("font-weight", "bold")
                    .text(year + i);
            }
        }

        var distrLabel = svg.append("g")
          .attr("transform", "translate(" + (xScale('2015-11') + xScale.rangeBand() + 45) + "," + (barHeight / 2) + ")")
          .attr("class", "distrLabel");

        if (config.mode == 'all') {
            distrLabel.append("text")
            .attr("dy", ".35em")
            .style("text-anchor", "middle")
            .attr("font-size", "12px")
            .attr("font-weight", "bold")
            .text("All");
        }
        else {
            distrLabel.append("text")
            .attr("dy", ".35em")
            .style("text-anchor", "middle")
            .attr("font-size", "12px")
            .attr("font-weight", "bold")
            .text("District " + d.key);
        }

    }

    type = function (d) {
        d['Event.Clearance.Date'] = dateFormat.parse(d['Event.Clearance.Date']);
        return d;
    }

    clear = function (deep) {
        $(config.container).empty();
        config.mode = null;
        if (deep == true) {
            config.data = null;
        }
    }

    redraw = function (mode) {
        clear(false);
        config.mode = mode;
        plotBars(config.data)
    }

    draw = function (mode) {
        clear();
        if (mode && mode !== 'all') {
            config.mode = 'district';
        }
        else {
            config.mode = 'all';
        }
        d3.csv("static/data/crime.csv", type, function (error, data) {
            if (error) throw error;
            plotBars(data);
        });
    }

    initModule = function (container) {
        config.container = container;

    }


    return {
        initModule: initModule,
        draw: draw,
        redraw: redraw,
        clear: clear
    }

})();