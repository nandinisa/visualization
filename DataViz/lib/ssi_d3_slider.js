﻿ssi.d3.slider = (function () {
    var initModule;

    initModule = function (container) {
        var margin = { top: 0, right: 40, bottom: 200, left: 40 },
     width = 960 - margin.left - margin.right,
     height = 300 - margin.top - margin.bottom;

        var dateFormat = d3.time.format("%m");
        var x = d3.time.scale()
            .domain([new Date(2009, 1, 1), new Date(2015, 6, 1)])
            .range([0, width]);

        var brush = d3.svg.brush()
            .x(x)
            .extent([new Date(2009, 1, 1), new Date(2015, 1, 1)])
            .on("brush", brushed);

        var svg = d3.select(container).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
          .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        svg.append("rect")
            .attr("class", "grid-background")
            .attr("width", width)
            .attr("height", height);

        svg.append("g")
            .attr("class", "x grid")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.svg.axis()
                .scale(x)
                .orient("bottom")
                .ticks(d3.time.month, 6)
                .tickSize(-height)
                .tickFormat(''))
          .selectAll(".tick")
            .classed("minor", function (d) {
                return d.getMonth();
            });

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.svg.axis()
              .scale(x)
              .orient("bottom")
              .ticks(d3.time.year)
              .tickPadding(0))
          .selectAll("text")
            .attr("x", 6)
            .style("text-anchor", null);

        var gBrush = svg.append("g")
            .attr("class", "brush")
            .call(brush);

        gBrush.selectAll("rect")
            .attr("height", height);

        function brushed() {
            var extent0 = brush.extent(),
                extent1;

            // if dragging, preserve the width of the extent
            if (d3.event.mode === "move") {
                var d0 = d3.time.year.round(extent0[0]),
                    d1 = d3.time.year.offset(d0, Math.round((extent0[1] - extent0[0]) / 864e5));
                extent1 = [d0, d1];
            }

                // otherwise, if resizing, round both dates
            else {
                extent1 = extent0.map(d3.time.year.round);

                // if empty when rounded, use floor & ceil instead
                if (extent1[0] >= extent1[1]) {
                    extent1[0] = d3.time.year.floor(extent0[0]);
                    extent1[1] = d3.time.year.ceil(extent0[1]);
                }
            }

            d3.select(this).call(brush.extent(extent1));

            //console.log(extent0[1]);
            //var data = [];
            //for (var i = extent1[0]; i < extent1[1]; i++) {
            //    data.push(i);
            //}

            //$.gevent.publish('aggr-time-filter', data);
        }
    }

    return {
        initModule : initModule
    }
    
})();