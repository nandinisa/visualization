// SSI variable
var ssi = (function () {
    var config = {
        $container : null, 
        mode: null,
        view : null
    }

    var initModule, load, draw, reload, drawLandmarks,
        getEventName;

    getEventName = function (type, mode) {
        return type + '-' + mode + '-data';
    }

    initModule = function (container) {
        //$container = container;
        config.view = 'overview';

        ssi.d3.choropleth.initModule('#map');
        ssi.d3.barChart.initModule('#graph');
        ssi.d3.areaChart.initModule('#areaGraph');
        $('#areaChart').hide();
        $('.graph').hide();
        // Add navigation functionality
        $('div.graph').find('button').click(function (event) {
            load($(this).attr("id"));
        });
    };

    reload = function (id, code) {
        $('.nav-tabs').find('li').attr('class', '');
        $('.nav-tabs').find('a').attr('aria-expanded', 'false');
        var anchor = $('#'+ id);
        $(anchor).parent().attr('class', 'active');
        $(anchor).attr('aria-expanded', 'true');

        config.view = id;
        ssi.d3.choropleth.clear();
        ssi.d3.barChart.clear(true);
        ssi.d3.areaChart.clear();

        $('.container-fluid').hide();
        $('.graph').hide();
        $('#areaChart').hide();
        //$('div.mapnavigator').show();
        if (id == 'overview') {
            $('.container-fluid').show();
            draw('district');
        }
        else if (id == 'details') {
            $('.graph').show();
            ssi.d3.barChart.draw(code);
        }
        else if (id == 'analyses') {
            //$('div.mapnavigator').hide();

            $('#areaChart').show();
            ssi.d3.areaChart.load();
        }
    }

    load = function (selectMode) {
        config.mode = selectMode;
        if (config.view == 'overview') {
            $.gevent.subscribe($('.container-fluid'), getEventName('aggr', selectMode), draw);
            ssi.data.loadData('aggr', selectMode);
        }
        else if (config.view == 'details') {
            ssi.d3.barChart.redraw(config.mode);
        }
    }

    draw = function () {
        $.gevent.unsubscribe($('.container-fluid'), getEventName('aggr', config.mode), draw);

        // Draw landmarks only after map is drawn.
        $.gevent.subscribe($('.container-fluid'), getEventName('landmarks', config.mode), drawLandmarks);
        ssi.data.loadData('landmarks', config.mode);
    };

    drawLandmarks = function () {
        $.gevent.unsubscribe($('.container-fluid'), getEventName('landmarks', config.mode), drawLandmarks);
        ssi.d3.choropleth.draw(config.mode);

        //ssi.d3.choropleth.drawLandmarks(config.mode);
    };

    return {
        initModule: initModule,
        load: load,
        reload: reload
    }
})();
