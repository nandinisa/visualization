ssi.d3 = {
    choropleth: (function () {
        var config = {
            map: null,
            svg: null,
            g: null,
            container: null,
            mapJson: null,
            mode: null,
            defaultColour: '#f0f0f0',
            defaultStroke: '#ffffff',
            highlightStroke: '#000000'
        };

        var util = {
            color: ['#fc9272', '#de2d26'],
            district: {
                color: ['#fc9272', '#de2d26'],
                mapUrl: 'static/shapes/city-council-districts.geojson'
            },
            zip: {
                color: ['#fee5d9', '#99000d'],
                mapUrl: 'static/shapes/zip-codes.geojson'
            },
            census :{
                color: ['#fee5d9', '#99000d'],
                mapUrl: 'static/shapes/census-tracts.geojson'
            }
        };

        var initModule, draw, drawLandmarks, clear, init,
            load, loadMap, onError, updateInfoBubbleVisibility,
            drawCrimeCount, getEventName, registerEvents, deregisterEvents;

        var drawMaps, drawMap, drawChoropleth, appendTooltip;

        var onMapClick = function (d) {
            highlightPathById(d.properties.district, 'District', d.properties.value);
        }

        var onMapElementClick = function (d) {
            highlightPathById(d.district, 'District', d.count);
        }

        var highlightPathById = function (id, category, count) {
            d3.selectAll("#unitPath").each(function (d, i) {
                if (d.properties.district == id) {
                    d3.select(this).attr('class', 'highlight-boundary');
                }
                else {
                    d3.select(this).attr('class', 'boundary');
                }
            });

            var mapNavigator = d3.select(".district");
            //mapNavigator.empty();
            var infoPanel = mapNavigator.select('#infoPanelG');
            infoPanel.remove();
            var imgPanel = mapNavigator.select('#imagePanel');
            imgPanel.remove();

            mapNavigator.append('g').attr('id', 'infoPanelG')
                .append('foreignObject')
                //.append('xhtml:body')
                .style('font', '14px "Helvetica Neue"')
                .style('margin-top', '5px')
                .html('<h5> In focus: ' + category + ' ' + id + ' </h5><h5> Number of crimes: ' + count + ' </h5>');
            mapNavigator.append('g').attr('id', 'imagePanel')
                .append('img')
                .attr('src', 'static/icons/district' + id + '.png')
                .attr('height', '150')
                .attr('width', '700')
                .on('dblclick', function (e) {
                    // Pass the district id
                    ssi.reload('details', id);
                })
                .on("mouseover", function () {
                    d3.select(this).style('cursor', 'pointer');
                })
                .on("mouseout", function () {
                    d3.select(this)
                        .style("cursor", "pointer");
                });
            //mapNavigator.append(img);
        }

        drawCrimeCount = function (filteredCrime) {
            //$.gevent.unsubscribe($('.container-fluid'), getEventName('aggr', config.mode), drawCrimeCount);

            config.svg.selectAll('.infoCircle').remove();
            config.svg.selectAll('.infoText').remove();           

            var colPaths = d3.selectAll("#unitPath");

            // defines color range
            var colorsGen = d3.scale.linear()
                .range(util.color)
                .domain([
                    d3.min(filteredCrime, function (d) {
                        return d.count;                 55
                    }),
                    d3.max(filteredCrime, function (d) {
                        return d.count;
                    })
                ]);

            var totalCrime = 0;
            // traverse through each of the aggregated crime count data
            // get the district of crime from the data given by: item[config.mode]
            // traverse through each element in the colpath : svg path element district boundary
            // map the district value to the data value. 
            // if it matches to the district.
            // then assign the value of crime count to it 
            $.each(filteredCrime, function (i, item) {
                colPaths.each(function (j) {
                    // Append the circles to the respective districts
                    if (j.properties[config.mode] == item[config.mode]) {
                        j.properties.value = item.count;

                        totalCrime += item.count;
                        //http://stackoverflow.com/questions/12062561/calculate-svg-path-centroid-with-d3-js
                        function getBoundingBoxCenter(selection) {
                            // get the DOM element from a D3 selection
                            // you could also use "this" inside .each()
                            var element = selection.node(),
                                // use the native SVG interface to get the bounding box
                                bbox = element.getBBox();
                            // return the center of the bounding box
                            return [bbox.x + bbox.width / 2, bbox.y + bbox.height / 2];
                        }

                        var tooltip = config.g.select(".districtTooltip");
                        var circle = config.g.append("circle")
                           .attr("class", "infoCircle")
                           .style("stroke", "black")
                           .style("opacity", .6)
                           .on('click', function (d) {
                               onMapElementClick(item);
                           })
                           .on("mouseover", function () {
                               tooltip.style("display", "block")
                               .style('cursor', 'pointer');
                           })
                           .on("mouseout", function () {
                               tooltip.style("display", "none");
                               d3.select(this)
                                   .style("cursor", "pointer");
                           })
                           .on("mousemove", function (d) {
                               var xPosition = d3.mouse(this)[0];
                               var yPosition = d3.mouse(this)[1];
                               var displayText = [];
                               displayText.push(' ');
                               displayText.push(' ');
                               displayText.push('  District: ' + item.district);
                               displayText.push('  Crime count:' + item.count);
                           
                               var text = tooltip.select('.tooltipText'),
                               yAttr = text.attr("y"),
                               lineHeight = 0.2,
                               dy = parseFloat(text.attr("dy"));
                           
                               tooltip.attr("transform", "translate(" + xPosition + "," + yPosition + ")");
                               tooltip.selectAll('tspan').remove();
                               //http://bl.ocks.org/mbostock/7555321
                               $.each(displayText, function (i, t) {
                                   var tspan = text.append("tspan").attr("x", 0).attr("y", yAttr).attr("dy", ++i * lineHeight + dy + "em").text(t);
                               });
                           });

                        var textInfo = config.g.append("text")
                                        .attr("class", "infoText")
                                        .style("font-weight", "bold")
                                        .on('click', function (d) {
                                            onMapElementClick(item);
                                        });

                        var self = this;
                        function circleUpdate() {
                            var pathSelection = d3.select(self);

                            var pathBounds = getBoundingBoxCenter(pathSelection);
                            var center_x = pathBounds[0];
                            var center_y = pathBounds[1];
                            var scaleRadius = d3.extent(filteredCrime, function (d) {
                                return d.count;
                            });

                            var circleRadius = d3.scale.sqrt()
                                    .domain(scaleRadius)
                                    .range([5, 10]);                            
                            
                            var colorsGen1 = d3.scale.ordinal().domain([1, 2, 3, 4, 5, 6, 7])
                                        .range(["red", "yellow", "green", "#dbdb8d", "orange", "#aec7e8", "white"]);

                            // Scales to the optimal size by zoom level. "Math.pow(2, 8 + config.map.getZoom()) / 2 / Math.PI" is the leaflet scaling
                            // Refer: http://stackoverflow.com/questions/21859953/matching-leaflet-zoom-level-with-d3-geo-scale/36330818
                            var scalingFactor = (Math.pow(2, 8 + config.map.getZoom()) / 2 / Math.PI) / 8000;
                            var calculatedCr = circleRadius(item.count * scalingFactor);
                            //var calculatedColor = colorsGen(j.properties.district);
                            config.g.selectAll(".infoCircle").each(function (d, i) {
                                if (i == (j.properties.district - 1)) {
                                    d3.select(this)
                                       .style("fill", colorsGen(j.properties.value))
                                       .attr('cx', center_x)
                                       .attr('cy', center_y)
                                       .attr("r", calculatedCr);
                                }
                            });

                            config.g.selectAll(".infoText").each(function (d, i) {
                                if (i == (j.properties.district - 1)) {
                                    d3.select(this)
                                    .attr("text-anchor", "middle")
                                    .text(function (d) { return Math.round(j.properties.value / 1000) + "K" })
                                    .attr("transform", function (d) { return "translate(" + center_x + "," + center_y + ")"; });
                                }
                            });

                            updateInfoBubbleVisibility();
                        }

                        config.map.on("viewreset", circleUpdate);
                        circleUpdate();
                        return false;
                    }
                });
            });

            
            // Display sum
            highlightPathById('all', 'district', totalCrime);
        }

        updateInfoBubbleVisibility = function () {
            var infoBubbleVisibilityZoomDelta = 2;
            var currentZoom = config.map.getZoom();
            if ((currentZoom > config.defaultMapZoomLevel + infoBubbleVisibilityZoomDelta) ||
                (currentZoom < config.defaultMapZoomLevel - infoBubbleVisibilityZoomDelta)) {
                config.g.selectAll(".infoText").each(function (d, i) {
                    d3.select(this)
                    .style("visibility", "hidden")
                });
                config.g.selectAll(".infoCircle").each(function (d, i) {
                    d3.select(this)
                    .style("visibility", "hidden")
                });
            }
            else {
                config.g.selectAll(".infoCircle").each(function (d, i) {
                    d3.select(this)
                    .style("visibility", "visible")
                });
                config.g.selectAll(".infoText").each(function (d, i) {
                    d3.select(this)
                    .style("visibility", "visible")
                });
            }
        }
       
        // Draw the default map with default settings
        drawMap = function (json, container) {
            // Clear previous settings
            config.svg.selectAll('#unitPath').remove();
            config.svg.selectAll('.infoCircle').remove();
            config.svg.selectAll('.infoText').remove();
            config.svg.selectAll('#landmarkCircle').remove();

            var tooltip = config.g.append("g")
                .attr("class", "districtTooltip")
                .style("display", "none");

            tooltip.append("rect")
              .attr("width", 140)
              .attr("height", 70)
              .attr("rx", 20)
              .attr("ry", 20)
              .attr("fill", "lightgray")
              .attr('stroke', 'white')
              .attr('stroke-width', 1)
              .style("opacity", 0.9);

            tooltip.append("text")
              .attr("class", "tooltipText")
              .attr("x", 15)
              .attr("dy", "1.2em")
              .attr("font-size", "13px")
              .attr("font-weight", "bold")
              .attr('font-color', 'black')
              .attr('margin-left', '10px');

            var feature = config.g.selectAll("path")
			    .data(json.features)
			    .enter().append("path")
			    .attr("class", "boundary")
                .attr("id", "unitPath")
                .on('click', function (d) {
                    onMapClick(d);
                })
                .on("mouseover", function () {
                    tooltip.style("display", "block")
                    .style('cursor', 'pointer');
                })
                .on("mouseout", function () {
                    tooltip.style("display", "none");
                    d3.select(this)
                        .style("cursor", "pointer");
                })
                .on("mousemove", function (d) {
                    var xPosition = d3.mouse(this)[0];
                    var yPosition = d3.mouse(this)[1];
                    var displayText = [];
                    displayText.push(' ');
                    displayText.push(' ');
                    displayText.push('  District: ' + d.properties.district);
                    displayText.push('  Crime count:' + d.properties.value);

                    var text = tooltip.select('text'),
                    yAttr = text.attr("y"),
                    lineHeight = 0.2,
                    dy = parseFloat(text.attr("dy"));

                    tooltip.attr("transform", "translate(" + xPosition + "," + yPosition + ")");
                    tooltip.selectAll('tspan').remove();
                    //http://bl.ocks.org/mbostock/7555321
                    $.each(displayText, function (i, t) {
                        var tspan = text.append("tspan").attr("x", 0).attr("y", yAttr).attr("dy", ++i * lineHeight + dy + "em").text(t);
                    });
                });
                //.on("mouseover", function (d, i) {
                //    //                d3.select(this).attr("d", path).style("fill", "#636363");
                //    tooltip.transition()
                //         .duration(200)
                //         .style("display", "block");
                //    tooltip.html("Location " + JSON.stringify(d.properties) + "<br/>")
                //         .style("left", (d3.event.pageX + 5) + "px")
                //         .style("top", (d3.event.pageY - 28) + "px");
                //})
                //.on("mouseout", function (d) {
                //    //d3.select(this).attr("d", path).style("fill", "f0f0f0");
                //    tooltip.transition()
                //        .duration(500)
                //        .style("display", "none");
                //});

            // Use Leaflet to implement a D3 geometric transformation.
            var projectPoint = function (x, y) {
                var point = config.map.latLngToLayerPoint(new L.LatLng(y, x));
                this.stream.point(point.x, point.y);
            }

            var transform = d3.geo.transform({ point: projectPoint });
            var path = d3.geo.path().projection(transform);

            // Reposition the SVG to cover the features.
            function update() {
                var bounds = path.bounds(json),
				topLeft = bounds[0],
				bottomRight = bounds[1];

                config.svg.attr("width", bottomRight[0] - topLeft[0])
				.attr("height", bottomRight[1] - topLeft[1])
				.style("left", topLeft[0] + "px")
				.style("top", topLeft[1] + "px");

                config.g.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")");
                feature.attr("d", path);
            }

            //function highlightLayer(layerID) {
            //    map._layers['name' + LayerID].setStyle(highlight);
            //}

            config.map.on("viewreset", update);
            update();
        }

        loadSlider = function () {
            var margin = { top: 20, right: 20, bottom: 20, left: 20 },
               width = 300 - margin.left - margin.right,
               height = 75 - margin.top - margin.bottom;

            var data = d3.range(50).map(Math.random);

            var x = d3.scale.linear()
                .domain([2011, 2015])
                .range([0, width])
                .clamp(true);

            var y = d3.random.normal(height / 2, height / 8);

            var brush = d3.svg.brush()
                .x(x)
                .extent([2011, 2015])
                .on("brushend", brushended);

            var arc = d3.svg.arc()
                .outerRadius(height / 4)
                .startAngle(0)
                .endAngle(function (d, i) { return i ? -Math.PI : Math.PI; });

            var sliderG = config.sliderSvg
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            sliderG.append("g")
                .attr("class", "sliderAxis")
                .attr("transform", "translate(0," + height + ")")
                .call(
                    d3.svg.axis()
                    .scale(x)
                    .orient("top")
                    .tickFormat(function (d) { return d; })
                    .ticks(5));

            var brushg = sliderG.append("g")
                .attr("class", "slider")
                .call(brush);

            brushg.selectAll(".resize").append("path")
                .attr("transform", "translate(0," + (height + (height * 0.25)) + ")")
                .attr("d", arc);

            brushg.selectAll("rect")
                .attr("transform", "translate(0," + height + ")")
                .attr("height", height / 2);

            brushg.select(".background")
                .style("visibility", "visible")
                .attr("transform", "translate(0," + (height + 1.5) + ")")
                .attr("height", (height / 2) - 3);

            getEventName = function (type, mode) {
                return type + '-' + mode + '-data';
            }

            function brushended() {
                if (!d3.event.sourceEvent) return; // only transition after input
                var extent0 = brush.extent();
                var extent1 = extent0.map(function (d) { return Math.round(d); });
                d3.select(this).transition()
                    .call(brush.extent(extent1))
                    .call(brush.event);

                //$.gevent.subscribe($('.container-fluid'), getEventName('aggr', config.mode), drawFilteredCount);
                var dateArray = [];
                var minD = parseInt(extent1[0]);
                var maxD = parseInt(extent1[1]);
                for (var i = 0; (i + minD) <= maxD; i++) {
                    minD = minD + i;
                    dateArray.push(minD.toString());
                }
                var f = ssi.data.getData('aggr', config.mode, { date: dateArray }, false);
                drawCrimeCount(f);
            }

            //$.gevent.subscribe($('.container-fluid'), getEventName('aggr', config.mode), drawFilteredCount);
            drawCrimeCount(ssi.data.getData('aggr', config.mode, { date: ['all'], type: 'crime' }));
            
        }

        // Draw the choropleth maps here
        // For the aggregate data
        drawMaps = function (json) {
            drawMap(json, '#mapShell');

            var mapNavigator = d3.select(".sliderDiv");
            mapNavigator.selectAll('#sliderSvg').remove();
            config.sliderSvg = mapNavigator.append("svg").attr("id", "sliderSvg");
            loadSlider();
            
            // Draw the image tag
            $('.crimeHour').empty();
            var crimePanel = d3.select(".crimeHour");
            crimePanel.append('g').attr('id', 'crimeInfo')
                .append('foreignObject')
                //.append('xhtml:body')
                .style('font', '14px "Helvetica Neue"')
                .style('margin-top', '5px')
                .html('<h5> Type of crime based on hour of the day</h5>');
            crimePanel.append('g').attr('id', 'crimeImg')
                .append('img')
                .attr('src', 'static/icons/hour.png')
                .attr('height', '250')
                .attr('width', '700')
                .on('dblclick', function (e) {
                    // Pass the district id
                    ssi.reload('analyses');
                })
                .on("mouseover", function () {
                    d3.select(this).style('cursor', 'pointer');
                })
                .on("mouseout", function () {
                    d3.select(this)
                        .style("cursor", "none");
                });
            
            //var checkbox = $(":checkbox").get(0);
            //if (checkbox && checkbox.checked == true) {
            //    drawLandmarks(config.mode);
            //}
        }

        // Callback function on error
        onError = function (err) {
            console.log(err);
        }

        // Downloads the respective map
        loadMap = function (data) {
            // $.gevent.unsubscribe(
            //  $(config.container),
            //  getEventName('aggr', config.mode),
            //  loadMap
            //);
            ssi.util.getAsyncRequest(util[config.mode].mapUrl, null, drawMaps, onError);
        }

        // Load the navigators and maps
        draw = function (mode) {
            init();
            config.mode = mode;
            loadMap();
        }

        drawLandmarks = function (mode) {
            var filteredLandmarks = ssi.data.getData('landmarks', 'district', { date: ['all'], type: 'landmarks' });

            // Use Leaflet to implement a D3 geometric transformation.
            var projectPoint = function (x, y) {
                return config.map.latLngToLayerPoint(new L.LatLng(y, x));
            }

            config.g.selectAll("#landmarkCircle")
		        .data(filteredLandmarks).enter()
		        .append("circle")
                .attr("id", "landmarkCircle")
		        .attr("r", "3px")
		        .attr("fill-opacity", "0.5")
                .style("fill", "black")
                .on("mouseover", function () {
                    tooltip.style("display", null);
                    d3.select(this).attr("r", "15px")
                    .attr("fill-opacity", "1")
                    .style("cursor", "pointer")
                    .style("fill", "Red");
                })
                .on("mouseout", function () {
                    tooltip.style("display", "none");
                    d3.select(this)
                        .attr("fill-opacity", "0.5")
                        .attr("r", "3px")
                        .style("cursor", "none")
                        .style("fill", "black");
                })
                .on("mousemove", function (d) {
                    var xPosition = d3.mouse(this)[0];
                    var yPosition = d3.mouse(this)[1];
                    var displayText = [];
                    displayText.push(d.NAME);
                    displayText.push(d.ADDRESS);
                    displayText.push(d.district);

                    var text = tooltip.select('text'),
                    yAttr = text.attr("y"),
                    lineHeight = 0.2,
                    dy = parseFloat(text.attr("dy"));

                    tooltip.attr("transform", "translate(" + xPosition + "," + yPosition + ")");
                    tooltip.selectAll('tspan').remove();
                    //http://bl.ocks.org/mbostock/7555321
                    $.each(displayText, function (i, t) {
                        //var tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
                        var tspan = text.append("tspan").attr("x", 0).attr("y", yAttr).attr("dy", ++i * lineHeight + dy + "em").text(t);
                    });

                    d3.select(this).attr("r", "15px")
                    .attr("fill-opacity", "1")
                    .style("cursor", "pointer")
                    .style("fill", "Red");
                });

            var tooltip = config.g.append("g")
                .attr("class", "landmarkTooltip")
                .style("display", "none");

            tooltip.append("rect")
              .attr("width", 220)
              .attr("height", 70)
              .attr("fill", "white")
              .style("opacity", 0.6);

            tooltip.append("text")
              .attr("x", 15)
              .attr("dy", "1.2em")
              .attr("font-size", "12px")
              .attr("font-weight", "bold")
              .attr('font-color', 'black');

            var allLandmarks = d3.selectAll("#landmarkCircle");
            var landmarkCirclesUpdate = function () {
                allLandmarks.each(function (d, i) {
                    d3.select(this)
                        .attr("cx", function (d) {
                            return projectPoint(d.longitude, d.latitude).x;
                        })
		                .attr("cy", function (d) {
		                    return projectPoint(d.longitude, d.latitude).y;
		                })
                });
            }

            config.map.on("viewreset", landmarkCirclesUpdate);
            landmarkCirclesUpdate();
        }

        init = function () {
            $(config.container).show();
            //$('#map').attr('height', '600px')
            //    .attr('width', '800px');
            $('<div>')
                .attr('id', 'mapShell')
                .appendTo($(config.container));
            config.defaultMapZoomLevel = 11;
            config.map = new L.Map('mapShell', { center: [47.60, -122.33], zoom: config.defaultMapZoomLevel })
                .addLayer(new L.TileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"));
            config.svg = d3.select(config.map.getPanes().overlayPane).append("svg").attr('class', 'map');
            config.g = config.svg.append("g").attr("class", "leaflet-zoom-hide");
        }

        clear = function () {
            //config.map.eachLayer(function (layer) {
            //    config.map.removeLayer(layer);
            //});
            if (config.map) {

                config.map.clearAllEventListeners();
                //config.map._clearPanes();
                //config.map._clearControlPos();

                config.map.remove();

            }
            //config.map.clearLayers();
            config.map = null;
            config.svg = null;
            config.g = null;
            
            d3.selectAll('#sliderSvg').remove();
            d3.select(".sliderDiv").empty();
            d3.select("#infoPanelG").remove();
            d3.select("#imagePanel").remove();
            $('.crimeHour').empty();

            // Clear previous values
            $(config.container).empty();
            $(config.container).hide();
        }

        initModule = function (container) {
            config.container = container;
            $(":checkbox").on('click', function (e) {
                var checked = this.checked;
                // remove landmarks
                if (config.svg) {
                    config.svg.selectAll('#landmarkCircle').remove();
                }

                if (checked) {
                    drawLandmarks(config.mode);
                }
            });
        };

        return {
            initModule: initModule,
            draw: draw,
            drawLandmarks: drawLandmarks,
            clear: clear
        }
    })()
}