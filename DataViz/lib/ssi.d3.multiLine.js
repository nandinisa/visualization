﻿ssi.d3.multiline = (function () {
    // JavaScript source code
    // Ref: https://bl.ocks.org/mbostock/3884955
    // http://bl.ocks.org/weiglemc/6185069
    var margin = { top: 50, right: 50, bottom: 50, left: 250 }, padding = 20,
        width = 800 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom,
            initModule, setTitle, draw, drawData, drawGraph;
    var config = {
        container: null,
        mode: null
    }

    setTitle = function (svg) {
        svg.append("text")
        .attr("x", (width / 2))
        .attr("y", -16)
        .attr("text-anchor", "middle")
        .style("font-size", "20px")
        .style("font-style", "bold")
        .text(config.mode + ' count by differet districts');
    }

    //drawPoints = function (svg, data) {
       
    //}

    drawData = function (fdata, keys) {
        var data = [];

        var dateFormat = d3.time.format("%Y");
        // Format data
        fdata.forEach(function (d) {
            var newObj = {};
            newObj.date = dateFormat.parse(d.date);
            newObj.count = +d.count;
            newObj[config.mode] = d[config.mode];
            data.push(newObj);
            //d.date = dateFormat.parse(d.date);
            //d.count = +d.count;
        });

        // Time scale of x axis
        // Scale to the available width
        var xScale = d3.time.scale()
            .range([0, width])
            .domain(d3.extent(data, function (d) { return d.date; }));;

        // Range scale for y axis
        // Scale to the available height
        var yScale = d3.scale.linear()
            .range([height, 0])
            .domain(d3.extent(data, function (d) { return d.count; }));
        // Create x and y axis
        var xAxis = d3.svg.axis()
            .scale(xScale)
            .tickFormat(dateFormat)
            .orient("bottom");

        yAxis = d3.svg.axis()
            .scale(yScale)
            .orient('left');

        // Choose any default colouring schema
        var colour = d3.scale.category10();

        // Categorize data
        var formattedData = keys.map(function (name) {
            return {
                name: config.mode + name,
                values: data.filter(function (d) {
                    if (d[config.mode] == name) {
                        return {
                            date: d.date,
                            count: d.count
                        };
                    }

                    return null;
                })
            };
        });

        // Create the linear line
        var line = d3.svg.line()
            .x(function (d) { return xScale(d.date); })
            .y(function (d) { return yScale(d.count); })
            .interpolate('linear');

        var svg = d3.select(config.container).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
          .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        svg.append("g")
            .attr("class", "xaxis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
        .append("text")
            .attr("x", width / 2)
            .attr("y", 40)
            .style("text-anchor", "end")
            .style('font-size', '12px')
            .style("font-weight", "bold")
            .text("Years");

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
          .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".85em")
            .style("text-anchor", "end")
            .style('font-size', '12px')
            .style("font-weight", "bold")
            .text("Count");

        var shops = svg.selectAll(".keys")
            .data(formattedData)
            .enter().append("g")
            .attr("class", "keys");

        shops.append("path")
            .attr("class", "line")
            .attr("d", function (d) { return line(d.values) })
            .style("stroke", function (d) { return colour(d.name); });

        // add the tooltip area to the webpage
        var tooltip = d3.select("body").append("div")
            .attr("class", "tooltip")
            .style("opacity", 0);

        // Create dots and assign tooltip
        svg.selectAll(".dot")
            .data(data)
            .enter().append("circle")
              .attr("class", "dot")
              .attr("r", 3.5)
              .attr("cx", function (d) { return xScale(d.date); })
              .attr("cy", function (d) { return yScale(d.count); })
              .style("fill", function (d) { return colour("District " + d.district); })
              .on("mouseover", function (d) {
                  tooltip.transition()
                       .duration(200)
                       .style("opacity", .9);
                  tooltip.html("District " + d.district + "<br/> (" + dateFormat(d.date)
                    + ", " + d.count + ")")
                       .style("left", (d3.event.pageX + 5) + "px")
                       .style("top", (d3.event.pageY - 28) + "px");
              })
              .on("mouseout", function (d) {
                  tooltip.transition()
                       .duration(500)
                       .style("opacity", 0);
              });

        // draw legend
        var legend = svg.selectAll(".legend")
            .data(colour.domain())
            .enter().append("g")
            .attr("class", "legend")
            .attr("transform", function (d, i) { return "translate(0," + i * 20 + ")";  });
        //.attr("transform", function (d, i) { return "translate(height," + i * 20 + ")"; });

        // draw legend colored rectangles
        legend.append("rect")
            .attr("x", width - 18)
            .attr("width", 18)
            .attr("height", 18)
            .style("fill", colour);

        // draw legend text
        legend.append("text")
            .attr("x", width - 24)
            .attr("y", 9)
            .attr("dy", ".35em")
            .style("text-anchor", "end")
            .text(function (d) { return d; })

        setTitle(svg);
    }

    drawGraph = function (data) {
        //$.gevent.unsubscribe(
        //              $(config.container),
        //              'aggr-' + config.mode + '-data',
        //               onDataLoad
        //            );
        $(config.container).empty();
        var crimeData = ssi.data.getData('aggr', config.mode, {
            date:
                   data == null ? ['2010', '2011', '2012', '2014', '2015'] : data, type: 'crime'
        });
        var colData = ssi.data.getData('aggr', config.mode, {
            date:
                data == null ? ['2010', '2011', '2012', '2014', '2015'] : data, type: 'collision'
        });

        var uniqueKeys = {};
        var keys = [];
        $.each(crimeData, function (i, item) {
            if (!uniqueKeys.hasOwnProperty(item[config.mode])) {
                uniqueKeys[item[config.mode]] = '';
                keys.push(item[config.mode]);
            }
        });

        drawData(crimeData, keys);
        drawData(colData, keys);
    }

    draw = function (mode) { 
        
        $(config.container).empty();

        config.mode = mode;
        drawGraph();
    }

    initModule = function (container) {
        config.container = container;
        // Subscribe for data load event
        // Register for data load event
        $.gevent.subscribe($(config.container), 'aggr-time-filter', drawGraph);
    }

    return {
        initModule: initModule,
        draw: draw
    }
})();