﻿// Utility class
// Handles all http requests
ssi.util = (function () {
    var getRequest, postRequest, getAsyncRequest, postAsyncRequest;

    getRequest = function (url, params, successCallback, failureCallback) {
        $.get(url, params).done(successCallback).fail(failureCallback);
    }

    postRequest = function (url, params) {
        $.ajax({
            type: 'POST',
            url: url,
            data: JSON.stringify(params),
            success: success,
            dataType: 'json',
            async: false
        });
    }


    getAsyncRequest = function (url, params, successCallback, failureCallback) {
        $.ajax({
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            mimeType: "application/json",
            url: url,
            data: params
        }).done(successCallback).fail(failureCallback);
    }

    postAsyncRequest = function (url, params, successCallback, failureCallback) {
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            url: url,
            data: JSON.stringify(params)
        }).done(successCallback).fail(failureCallback);
    }

    return {
        getRequest: getRequest,
        getAsyncRequest: getAsyncRequest,
        postAsyncRequest: postAsyncRequest
    }
})();