// Data file
ssi.data = (function () {
    var config = {
        aggr : {
            district :{ url: 'static/data/district_aggr.json', data: null },
            zip : { url: 'static/data/zip_aggr.json', data: null },
            census : { url: 'static/data/census_aggr.json', data: null }
        },

        schools: { url: 'all_schools.csv', data: null },
        collisions: { url: 'static/data/collisions.json', data: null },
        permits: { url: 'static/data/land_use_permits.csv', data: null },
        landmarks: {
            district: { url: 'static/data/landmarks.json', data: null }
        },
        parks: { url: 'static/data/parks_recreations.csv', data: null },
        police: { url: 'static/data/police_response.csv', data: null },
        traffic: { url: 'static/data/traffic_volume.csv', data: null }
    };

    var getDataUrl, getData, setData,
        loadData, onError;

    getDataUrl = function (type, category) {
        return config[type][category].url;
    }
   
    getData = function (type, category, filter, distinct) {
        var data = config[type][category].data;

        if (filter) {
            //filter.date = ['2010', '2011'];
            //distinct = false;
            var collate = (distinct != null && distinct === false);
            
            // date type filtering
            if (filter.type == 'collision') {
                data = $.map(config[type][category].data[0].collision, function (item, i) {
                    if ($.inArray(item.date, filter.date) > -1)
                        return item;
                    return null;
                });
            }
	        else if (filter.type == 'landmarks') {
                var data = $.map(config[type][category].data, function (item, i) {             
                    return item;
                });
            }
            else {
                data = $.map(config[type][category].data[1].police_response, function (item, i) {
                    if ($.inArray(item.date, filter.date) > -1)
                        return item;
                    return null;
                });

                if (collate == false) {
                    return data;
                }
                else {
                    var key = filter.date.length > 1 ? filter.date[0] + '-' + filter.date[filter.date.length - 1] : filter.date[0];
                    var collatedData = {};
                    $.each(data, function (i, d) {
                        if (collatedData[d[category]]) {
                            var item = collatedData[d[category]];
                            item['count'] = item['count'] + d.count;
                        }
                        else {
                            collatedData[d[category]] = { 'count': d.count, 'date': key };
                        }
                    });

                    var data = [];
                    for (var property in collatedData) {
                        if (collatedData.hasOwnProperty(property)) {
                            var item = { 'count': collatedData[property].count, 'date': collatedData[property].date };
                            item[category] = property;
                            data.push(item);
                        }
                    }
                }
                
            }

            return data;
            //return sort(data, function (a, b) { return a - b });
        }

        return data;
    }

    setData = function (type, category, data) {
        config[type][category].data = data;
    }

    loadData = function (type, category) {
        var data = getData(type, category);
        if (data == null) {
            ssi.util.getAsyncRequest(ssi.data.getDataUrl(type, category), null, function (data, responseTxt, response, err) {
                if (err) {
                    onError(err);
                    return;
                }

                setData(type, category, data);
                
                // publish event with data
                $.gevent.publish(
                      type + '-' + category + '-data',
                      data
                    );
            }, onError);
        }
        else {
            // publish event with data
            $.gevent.publish(
                  type + '-' + category + '-data',
                  data
                );
        }
    }

    // Callback function on error
    onError = function (err) {
        console.log(err);
    }

    return {
        config: config,
        getDataUrl: getDataUrl,
        getData: getData,
        setData: setData,
        loadData: loadData

    }
})();
